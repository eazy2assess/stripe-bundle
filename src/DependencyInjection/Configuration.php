<?php

namespace Eazy\Bundle\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package Eazy\Bundle\PaymentBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('eazy_payment');
        $rootNode    = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->scalarNode('stripe_api_key')->defaultNull()->end()
            ->scalarNode('stripe_endpoint_secret')->defaultNull()->end()
            ->end();

        return $treeBuilder;
    }
}
