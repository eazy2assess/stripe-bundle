<?php


namespace Eazy\Bundle\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class EazyPaymentExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $container->setParameter('eazy_payment.stripe_api_key', $config['stripe_api_key']);
        $container->setParameter('eazy_payment.stripe_endpoint_secret', $config['stripe_endpoint_secret']);

        $filelocator = new FileLocator(__DIR__ . '/../Resources/config');
        $yamlLoader  = new Loader\YamlFileLoader($container, $filelocator);
        $yamlLoader->load('services.yaml');
    }
}
