<?php

namespace Eazy\Bundle\PaymentBundle\Dto;

/**
 * Interface CreditCardDtoInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Dto
 */
interface CreditCardDtoInterface
{
    /**
     * @return string|null
     */
    public function getCardNumber(): ?string;

    /**
     * @return string|null
     */
    public function getCvc(): ?string;

    /**
     * @return int|null
     */
    public function getMonth(): ?int;

    /**
     * @return int|null
     */
    public function getYear(): ?int;

    /**
     * @return bool
     */
    public function isDefault(): bool;
}
