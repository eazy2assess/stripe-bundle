<?php

namespace Eazy\Bundle\PaymentBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class EazyPaymentBundle
 *
 * @package Eazy\Bundle\PaymentBundle
 */
class EazyPaymentBundle extends Bundle
{
}
