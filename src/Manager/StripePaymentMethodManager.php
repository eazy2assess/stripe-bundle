<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Dto\CreditCardDtoInterface;
use Eazy\Bundle\PaymentBundle\Factory\PaymentMethodFactoryInterface;
use Eazy\Bundle\PaymentBundle\Model\CustomerInterface;
use Eazy\Bundle\PaymentBundle\Model\PaymentMethodInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;

/**
 * Class StripePaymentMethodManager
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
class StripePaymentMethodManager implements PaymentMethodManagerInterface
{
    /** @var string  */
    private $stripeApiKey;

    /** @var PaymentMethodFactoryInterface */
    private $factory;

    /**
     * StripePaymentMethodManager constructor.
     *
     * @param string                        $stripeApiKey
     * @param PaymentMethodFactoryInterface $factory
     */
    public function __construct(
        string $stripeApiKey,
        PaymentMethodFactoryInterface $factory
    ) {
        $this->stripeApiKey = $stripeApiKey;
        $this->factory      = $factory;
    }

    /**
     * @param CustomerInterface      $customer
     * @param CreditCardDtoInterface $creditCardDto
     *
     * @return PaymentMethodInterface
     * @throws ApiErrorException
     */
    public function add(
        CustomerInterface $customer,
        CreditCardDtoInterface $creditCardDto
    ) : PaymentMethodInterface {
        $response = PaymentMethod::create(
            [
                'type' => 'card',
                'card' => [
                    'number'    => $creditCardDto->getCardNumber(),
                    'exp_month' => $creditCardDto->getMonth(),
                    'exp_year'  => $creditCardDto->getYear(),
                    'cvc'       => $creditCardDto->getCvc(),
                ]
            ],
            [
                'api_key' => $this->stripeApiKey
            ]
        );

        $response->attach(
            ['customer' => $customer->getCustomerId()],
            ['api_key' => $this->stripeApiKey]
        );

        return $this->factory->create(
            $response->id,
            'card',
            $response->toArray(),
            $creditCardDto->isDefault()
        );
    }

    /**
     * {@inheritDoc}
     * @throws ApiErrorException
     */
    public function attach(
        CustomerInterface $customer,
        PaymentMethodInterface $paymentMethod
    ): void {
        $response = PaymentMethod::retrieve(
            $paymentMethod->getPaymentMethodId(),
            ['api_key' => $this->stripeApiKey]
        );

        $response->attach(
            ['customer' => $customer->getCustomerId()],
            ['api_key' => $this->stripeApiKey]
        );
    }
}
