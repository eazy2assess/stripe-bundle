<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Dto\CreditCardDtoInterface;
use Eazy\Bundle\PaymentBundle\Model\CustomerInterface;
use Eazy\Bundle\PaymentBundle\Model\PaymentMethodInterface;

/**
 * Interface PaymentMethodManagerInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
interface PaymentMethodManagerInterface
{
    /**
     * @param CustomerInterface      $customer
     * @param CreditCardDtoInterface $creditCardDto
     *
     * @return PaymentMethodInterface
     */
    public function add(
        CustomerInterface $customer,
        CreditCardDtoInterface $creditCardDto
    ): PaymentMethodInterface;

    /**
     * @param CustomerInterface      $customer
     * @param PaymentMethodInterface $creditCardDto
     */
    public function attach(
        CustomerInterface $customer,
        PaymentMethodInterface $creditCardDto
    ): void;
}
