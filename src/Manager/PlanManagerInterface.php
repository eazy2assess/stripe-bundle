<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Model\PlanInterface;

/**
 * Interface PlanManagerInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
interface PlanManagerInterface
{
    /**
     * @param PlanInterface $plan
     *
     * @return array
     */
    public function create(PlanInterface $plan) : array;

    /**
     * @param PlanInterface $plan
     *
     * @return array
     */
    public function createOrUpdate(PlanInterface $plan) : array;

    /**
     * @param PlanInterface $plan
     *
     * @return array
     */
    public function update(PlanInterface $plan) : array;

    /**
     * @param PlanInterface $plan
     */
    public function remove(PlanInterface $plan) : void;
}
