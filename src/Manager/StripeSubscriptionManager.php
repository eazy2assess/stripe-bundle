<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Exception\StripeSubscriptionDoesNotExistsException;
use Eazy\Bundle\PaymentBundle\Exception\StripeSubscriptionExistsException;
use Eazy\Bundle\PaymentBundle\Model\SubscriptionInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\Subscription;

/**
 * Class StripeSubscriptionManager
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
class StripeSubscriptionManager implements SubscriptionManagerInterface
{
    /** @var string  */
    private $stripeApiKey;

    /**
     * StripeCustomerManager constructor.
     *
     * @param string $stripeApiKey
     */
    public function __construct(string $stripeApiKey)
    {
        $this->stripeApiKey = $stripeApiKey;
    }

    /**
     * @param SubscriptionInterface $subscription
     *
     * @return array|null
     * @throws ApiErrorException
     * @throws StripeSubscriptionExistsException
     */
    public function create(SubscriptionInterface $subscription) : ?array
    {
        if (!empty($subscription->getSubscriptionId())) {
            throw new StripeSubscriptionExistsException();
        }

        return Subscription::create(
            [
                'customer' => $subscription->getCustomerId(),
                'items' => [
                    [
                        'plan' => $subscription->getPlanId()
                    ]
                ],
                'trial_from_plan' => SubscriptionInterface::STATUS_TRIAL === $subscription->getSubscriptionStatus(),
                'default_payment_method' => $subscription->getPaymentMethodId()
            ],
            ['api_key' => $this->stripeApiKey]
        )->toArray();
    }

    /**
     * @param SubscriptionInterface $subscription
     *
     * @throws ApiErrorException
     * @throws StripeSubscriptionDoesNotExistsException
     */
    public function remove(SubscriptionInterface $subscription) : ?array
    {
        if (empty($subscription->getSubscriptionId())) {
            throw new StripeSubscriptionDoesNotExistsException();
        }

        return Subscription::update(
                $subscription->getSubscriptionId(),
                ['cancel_at_period_end' => true],
                ['api_key' => $this->stripeApiKey]
            )
            ->toArray();
    }

    /**
     * @param SubscriptionInterface $subscription
     *
     * @throws ApiErrorException
     * @throws StripeSubscriptionDoesNotExistsException
     *
     * @see https://stripe.com/docs/billing/subscriptions/cancel#reactivating-canceled-subscriptions
     */
    public function reactivate(SubscriptionInterface $subscription) : ?array
    {
        if (empty($subscription->getSubscriptionId())) {
            throw new StripeSubscriptionDoesNotExistsException();
        }

        return Subscription::update(
                $subscription->getSubscriptionId(),
                ['cancel_at_period_end' => false],
                ['api_key' => $this->stripeApiKey]
            )
            ->toArray();
    }
}
