<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Model\CustomerInterface;

/**
 * Interface CustomerManagerInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
interface CustomerManagerInterface
{
    /**
     * @param CustomerInterface $customer
     *
     * @return array
     */
    public function create(CustomerInterface $customer) : array;

    /**
     * @param CustomerInterface $customer
     *
     * @return array
     */
    public function update(CustomerInterface $customer) : array;

    /**
     * @param CustomerInterface $customer
     *
     * @return array
     */
    public function createOrUpdate(CustomerInterface $customer) : array;

    /**
     * @param CustomerInterface $customer
     */
    public function remove(CustomerInterface $customer) : void;
}
