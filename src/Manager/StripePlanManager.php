<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Model\PlanInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\Plan;

/**
 * Class StripePlanManager
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
class StripePlanManager implements PlanManagerInterface
{
    /** @var string  */
    private $stripeApiKey;

    /**
     * StripeCustomerManager constructor.
     *
     * @param string $stripeApiKey
     */
    public function __construct(string $stripeApiKey)
    {
        $this->stripeApiKey = $stripeApiKey;
    }

    /**
     * @param PlanInterface $plan
     *
     * @return array
     * @throws ApiErrorException
     */
    public function create(PlanInterface $plan) : array
    {
        $response = Plan::create(
            [
                'id'                => $plan->getPlanId(),
                'currency'          => $plan->getPlanCurrency(),
                'interval'          => $plan->getPlanInterval(),
                'nickname'          => $plan->getPlanDescription(),
                'amount'            => $plan->getPlanAmount(),
                'active'            => $plan->isPlanActive(),
                'trial_period_days' => $plan->getPlanTrialPeriodDays(),
                'product' => [
                    'name' => $plan->getPlanName()
                ]
            ],
            [
                'api_key' => $this->stripeApiKey
            ]
        );

        return $response->toArray();
    }

    /**
     * @param PlanInterface $plan
     *
     * @return array
     * @throws ApiErrorException
     */
    public function update(PlanInterface $plan) : array
    {
        return Plan::update(
                $plan->getPlanId(),
                [
                    'active' => $plan->isPlanActive(),
                    'nickname' => $plan->getPlanDescription()
                ],
                ['api_key' => $this->stripeApiKey]
            )
        ->toArray();
    }

    /**
     * @param PlanInterface $plan
     *
     * @return array
     * @throws ApiErrorException
     */
    public function createOrUpdate(PlanInterface $plan) : array
    {
        if (empty($plan->getPlanId())) {
            return $this->create($plan);
        }
        return $this->update($plan);
    }

    /**
     * @param PlanInterface $plan
     *
     * @throws ApiErrorException
     */
    public function remove(PlanInterface $plan) : void
    {
        Plan::retrieve(
            $plan->getPlanId(),
            ['api_key' => $this->stripeApiKey]
        )->delete();
    }
}
