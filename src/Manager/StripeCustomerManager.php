<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Exception\StripeCustomerDoesNotExistsException;
use Eazy\Bundle\PaymentBundle\Exception\StripeCustomerExistsException;
use Eazy\Bundle\PaymentBundle\Model\CustomerInterface;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;

/**
 * Class StripeCustomerManager
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
class StripeCustomerManager implements CustomerManagerInterface
{
    /** @var string  */
    private $stripeApiKey;

    /**
     * StripeCustomerManager constructor.
     *
     * @param string $stripeApiKey
     */
    public function __construct(string $stripeApiKey)
    {
        $this->stripeApiKey = $stripeApiKey;
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return array
     * @throws ApiErrorException
     * @throws StripeCustomerExistsException
     */
    public function create(CustomerInterface $customer) : array
    {
        if (!empty($customer->getCustomerId())) {
            throw new StripeCustomerExistsException();
        }

        $data = [
            'metadata' => $customer->getCustomerMetadata()
        ];

        if (!empty($customer->getCustomerEmail())) {
            $data['email'] = $customer->getCustomerEmail();
        }

        if (!empty($customer->getCustomerFullName())) {
            $data['name'] = $customer->getCustomerFullName();
        }

        if (!empty($customer->getCustomerPhone())) {
            $data['phone'] = $customer->getCustomerPhone();
        }

        if (!empty($customer->getCustomerDescription())) {
            $data['description'] = $customer->getCustomerDescription();
        }

        return Customer::create(
            $data,
            ['api_key' => $this->stripeApiKey]
        )->toArray();
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return array
     * @throws ApiErrorException
     */
    public function update(CustomerInterface $customer) : array
    {
        $model = Customer::retrieve(
            $customer->getCustomerId(),
            ['api_key' => $this->stripeApiKey]
        );

        if (!empty($customer->getCustomerEmail())) {
            $model->email = $customer->getCustomerEmail();
        }

        if (!empty($customer->getCustomerFullName())) {
            $model->name = $customer->getCustomerFullName();
        }

        if (!empty($customer->getCustomerPhone())) {
            $model->phone = $customer->getCustomerPhone();
        }

        if (!empty($customer->getCustomerDescription())) {
            $model->description = $customer->getCustomerDescription();
        }

        return $model->save(['api_key' => $this->stripeApiKey])->toArray();
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return array
     * @throws ApiErrorException
     * @throws StripeCustomerExistsException
     */
    public function createOrUpdate(CustomerInterface $customer) : array
    {
        if (empty($customer->getCustomerId())) {
            return $this->create($customer);
        }
        return $this->update($customer);
    }

    /**
     * @param CustomerInterface $customer
     *
     * @throws ApiErrorException
     * @throws StripeCustomerDoesNotExistsException
     */
    public function remove(CustomerInterface $customer) : void
    {
        if (empty($customer->getCustomerId())) {
            throw new StripeCustomerDoesNotExistsException();
        }

        Customer::retrieve(
            $customer->getCustomerId(),
            ['api_key' => $this->stripeApiKey]
        )->delete();
    }
}
