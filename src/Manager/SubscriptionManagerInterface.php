<?php

namespace Eazy\Bundle\PaymentBundle\Manager;

use Eazy\Bundle\PaymentBundle\Model\SubscriptionInterface;

/**
 * Interface SubscriptionManagerInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Manager
 */
interface SubscriptionManagerInterface
{
    /**
     * @param SubscriptionInterface $subscription
     *
     * @return array|null
     */
    public function create(SubscriptionInterface $subscription) : ?array;

    /**
     * @param SubscriptionInterface $subscription
     *
     * @return array|null
     */
    public function remove(SubscriptionInterface $subscription) : ?array;

    /**
     * @param SubscriptionInterface $subscription
     *
     * @return array|null
     */
    public function reactivate(SubscriptionInterface $subscription) : ?array;
}
