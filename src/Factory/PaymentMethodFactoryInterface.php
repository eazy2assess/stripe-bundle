<?php

namespace Eazy\Bundle\PaymentBundle\Factory;

use Eazy\Bundle\PaymentBundle\Model\PaymentMethodInterface;

/**
 * Interface PaymentMethodFactoryInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Factory
 */
interface PaymentMethodFactoryInterface
{
    /**
     * @param string $paymentMethodId
     * @param string $paymentMethodType
     * @param array  $paymentMethodData
     * @param bool   $isPaymentMethodDefault
     *
     * @return PaymentMethodInterface
     */
    public function create(
        string $paymentMethodId,
        string $paymentMethodType,
        array $paymentMethodData,
        bool $isPaymentMethodDefault
    ) : PaymentMethodInterface;
}
