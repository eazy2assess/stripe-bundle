<?php

namespace Eazy\Bundle\PaymentBundle\Factory;

use Eazy\Bundle\PaymentBundle\Model\EventInterface;

/**
 * Interface EventFactoryInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Factory
 */
interface EventFactoryInterface
{
    /**
     * @param string $eventId
     * @param string $eventType
     * @param array  $eventData
     *
     * @return EventInterface
     */
    public function create(
        string $eventId,
        string $eventType,
        array $eventData
    ) : EventInterface;
}
