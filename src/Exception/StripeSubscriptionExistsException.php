<?php

namespace Eazy\Bundle\PaymentBundle\Exception;

use Exception;
use Throwable;

/**
 * Class StripeSubscriptionExistsException
 *
 * @package Eazy\Bundle\PaymentBundle\Exception
 */
class StripeSubscriptionExistsException extends Exception
{
    /**
     * StripeSubscriptionExistsException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'StripeSubscription already exists', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
