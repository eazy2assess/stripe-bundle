<?php

namespace Eazy\Bundle\PaymentBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class SubscriptionExistsException
 *
 * @package Eazy\Bundle\PaymentBundle\Exception
 */
class SubscriptionExistsException extends HttpException
{
    /**
     * SubscriptionExistsException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     * @param int             $httpCode
     */
    public function __construct(
        $message = 'Subscription already exists.',
        $code = 0,
        \Exception $previous = null,
        $httpCode = 401
    ) {
        parent::__construct($httpCode, $message, $previous, array(), $code);
    }
}
