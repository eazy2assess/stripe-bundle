<?php

namespace Eazy\Bundle\PaymentBundle\Exception;

use Exception;
use Throwable;

/**
 * Class StripeCustomerExistsException
 *
 * @package Eazy\Bundle\PaymentBundle\Exception
 */
class StripeCustomerExistsException extends Exception
{
    /**
     * StripeCustomerExistsException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'StripeCustomer exists', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
