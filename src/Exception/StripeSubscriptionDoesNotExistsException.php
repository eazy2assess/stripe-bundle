<?php

namespace Eazy\Bundle\PaymentBundle\Exception;

use Exception;
use Throwable;

/**
 * Class StripeSubscriptionDoesNotExistsException
 *
 * @package Eazy\Bundle\PaymentBundle\Exception
 */
class StripeSubscriptionDoesNotExistsException extends Exception
{
    /**
     * StripeSubscriptionDoesNotExistsException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'StripeSubscription does not exists', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
