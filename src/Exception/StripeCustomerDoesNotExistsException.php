<?php

namespace Eazy\Bundle\PaymentBundle\Exception;

use Exception;
use Throwable;

/**
 * Class StripeCustomerDoesNotExistsException
 *
 * @package Eazy\Bundle\PaymentBundle\Exception
 */
class StripeCustomerDoesNotExistsException extends Exception
{
    /**
     * StripeCustomerDoesNotExistsException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'StripeCustomer does not exists', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
