<?php

namespace Eazy\Bundle\PaymentBundle\Controller;

use Eazy\Bundle\PaymentBundle\Factory\EventFactoryInterface;
use Eazy\Bundle\PaymentBundle\Model\EventInterface;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Stripe;
use Stripe\Webhook;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CreateStripeEventAction
 *
 * @package Eazy\Bundle\PaymentBundle\Controller
 */
class CreateStripeEventAction
{
    /** @var string */
    private const HEADER_STRIPE_SIGNATURE = 'stripe_signature';

    /** @var string */
    private $stripeApiKey;

    /** @var string */
    private $stripeEndpointSecret;

    /** @var EventFactoryInterface  */
    private $factory;

    /**
     * CreateStripeEventAction constructor.
     *
     * @param string                $stripeApiKey
     * @param string                $stripeEndpointSecret
     * @param EventFactoryInterface $factory
     */
    public function __construct(
        string $stripeApiKey,
        string $stripeEndpointSecret,
        EventFactoryInterface $factory
    ) {
        $this->stripeApiKey = $stripeApiKey;
        $this->stripeEndpointSecret = $stripeEndpointSecret;
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     *
     * @return EventInterface
     * @throws SignatureVerificationException
     */
    public function __invoke(
        Request $request
    ): EventInterface {
        Stripe::setApiKey($this->stripeApiKey);

        $event = Webhook::constructEvent(
            $request->getContent(),
            $request->headers->get(self::HEADER_STRIPE_SIGNATURE),
            $this->stripeEndpointSecret
        );

        return $this->factory->create(
            $event->id,
            $event->type,
            $event->data->object->toArray()
        );
    }
}
