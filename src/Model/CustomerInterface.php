<?php


namespace Eazy\Bundle\PaymentBundle\Model;

/**
 * Interface CustomerInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Model
 */
interface CustomerInterface
{
    /**
     * @return string|null
     */
    public function getCustomerId(): ?string;

    /**
     * @return array|null
     */
    public function getCustomerMetadata(): ?array;

    /**
     * @return string|null
     */
    public function getCustomerEmail(): ?string;

    /**
     * @return string|null
     */
    public function getCustomerFullName(): ?string;

    /**
     * @return string|null
     */
    public function getCustomerPhone(): ?string;

    /**
     * @return string|null
     */
    public function getCustomerDescription(): ?string;
}
