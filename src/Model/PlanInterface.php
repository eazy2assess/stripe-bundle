<?php

namespace Eazy\Bundle\PaymentBundle\Model;

/**
 * Interface SubscriptionPlanInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Model
 */
interface PlanInterface
{
    /** @var string */
    public const DEFAULT_CURRENCY = 'EUR';

    /** @var int */
    public const DEFAULT_TRIAL_PERIOD_DAYS = 30;

    /** @var array */
    public const INTERVALS = [self::INTERVAL_MONTH, self::INTERVAL_YEAR];

    /** @var string */
    public const INTERVAL_MONTH = 'month';

    /** @var string */
    public const INTERVAL_YEAR = 'year';

    /**
     * @return string|null
     */
    public function getPlanId(): ?string;

    /**
     * @return string|null
     */
    public function getPlanName(): ?string;

    /**
     * @return string|null
     */
    public function getPlanDescription(): ?string;

    /**
     * @return int|null
     */
    public function getPlanAmount(): ?int;

    /**
     * @return string|null
     */
    public function getPlanCurrency(): ?string;

    /**
     * @return int|null
     */
    public function getPlanTrialPeriodDays(): ?int;

    /**
     * @return string|null
     */
    public function getPlanInterval(): ?string;

    /**
     * @return int
     */
    public function getPlanIntervalCount(): int;

    /**
     * @return bool
     */
    public function isPlanActive(): bool;
}
