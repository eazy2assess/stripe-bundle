<?php


namespace Eazy\Bundle\PaymentBundle\Model;

interface EventInterface
{
    public function getEventId(): ?string;
    public function getEventType(): ?string;
    public function getEventData(): ?array;
}
