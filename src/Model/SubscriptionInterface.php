<?php

namespace Eazy\Bundle\PaymentBundle\Model;

/**
 * Interface SubscriptionInterface
 *
 * @package Eazy\Bundle\PaymentBundle\Model
 */
interface SubscriptionInterface
{
    /** @var string */
    public const STATUS_TRIAL     = 'trial';

    /** @var string */
    public const STATUS_ACTIVE    = 'active';

    /** @var string */
    public const STATUS_PAUSED    = 'paused';

    /** @var string */
    public const STATUS_CANCELED  = 'canceled';

    /** @var string */
    public const STATUS_UNPAID = 'unpaid';

    /** @var string */
    public const STATUS_PAST_DUE = 'past_due';

    /** @var string */
    public const STATUS_TRIALING = 'trialing';

    /** @var string */
    public const STATUS_INCOMPLETE = 'incomplete';

    /** @var string */
    public const STATUS_INCOMPLETE_EXPIRED = 'incomplete_expired';

    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_CANCELED,
        self::STATUS_PAUSED,
        self::STATUS_TRIAL,
        self::STATUS_INCOMPLETE,
        self::STATUS_INCOMPLETE_EXPIRED,
        self::STATUS_PAST_DUE,
        self::STATUS_UNPAID,
        self::STATUS_TRIALING
    ];

    /**
     * @return string|null
     */
    public function getSubscriptionId(): ?string;

    /**
     * @return string|null
     */
    public function getCustomerId(): ?string;

    /**
     * @return string|null
     */
    public function getPaymentMethodId(): ?string;

    /**
     * @return string|null
     */
    public function getPlanId(): ?string;

    /**
     * @return string|null
     */
    public function getSubscriptionStatus(): ?string;
}
