<?php


namespace Eazy\Bundle\PaymentBundle\Model;

use App\Entity\Client;
use App\Entity\ClientPaymentMethod;

interface PaymentMethodInterface
{
    /**
     * @return Client|null
     */
    public function getCustomerId(): ?string;


    /**
     * @return string|null
     */
    public function getPaymentMethodId(): ?string;

    /**
     * @return string|null
     */
    public function getPaymentMethodType(): ?string;

    /**
     * @return array
     */
    public function getPaymentMethodData(): array;

    /**
     * @return bool
     */
    public function isPaymentMethodDefault(): bool;
}
